const {assert} = require('chai');
const chai = require('chai');
const chaiHTTP = require('chai-http');
const expect = chai.expect;

chai.use(chaiHTTP); //use all the functionalities of chai-http

//API test suites
describe('API_Test_Suite', () => {
	const domain = 'http://localhost:5001';
	it('GET / endpoint', (done)=>{
		chai.request(domain) // accepts a server url to be used on the API unit testing
		.get('/')// .get() .post() .put() .delete() -> chai http request
		.end((error, res) => { //handles the response or an error that will be received from the endpoint
			assert.equal(res.status, 200);
			// expect(res.status).to.equal(200);
			done(); //to state that the test is done
		}) 
	})

	it('GET /users endpoint', (done)=>{
		chai.request(domain)
		.get('/users')
		.end((err, res) =>{
			expect(res).to.not.equal(undefined);
			done()
		})
	})

	it('POST /user endpoint', (done)=>{
		chai.request(domain)
		.post('/user')
		.type('json') //specifies the type of request req.body to be sent our as a part of the POST request
		.send({
			'fullName': 'Jason',
			'age': 45
		}) // specifies the data to sent as a part of the POST request
		.end((err, res) =>{
			expect(res.status).to.equal(200);
			done();
		})
	})

	it('POST /user endpoint fullName is missing', (done) => {
		chai.request(domain)
		.post('/user')
		.type('json')
		.send({
			'alias': "Jason",
			'age': 28
		})
		.end((err, res) =>{
			expect(res.status).to.equal(400); //Bad request
			done()
		})
	})
});