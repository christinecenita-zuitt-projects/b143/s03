const {user} = require('../data'); //mock data for our users
module.exports = (app) => {
	//GET API ENDPOINT
	app.get('/', (req, res) => {
		return res.status(200).send()
	})

	//GET API ENDPOINT
	app.get('/users', (req, res)=> {
		return res.send(users);
	})

	//POST API ENDPOINT
	app.post('/user', (req, res) => {
		/*
			To create a person we need fullName and age
		*/
		if(!req.body.hasOwnProperty('fullName')){
			return res.status(400).send({
				'Error': "Bad request - missing required parameter fullName"
			})
		}

		return res.status(200).send({
			'Message': 'User created!'
		})
	});
}

